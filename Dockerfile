FROM python:3.7-slim-stretch

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED=1

ENV DJANGO_DIR=/geoinfo

RUN mkdir $DJANGO_DIR

WORKDIR $DJANGO_DIR

RUN pip install --upgrade pip

COPY requirements.txt $DJANGO_DIR/

RUN pip install -r requirements.txt

COPY . $DJANGO_DIR/