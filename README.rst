GEOINFO
===============

Backend on Django with Docker

Requirements
----------------

* `Docker <https://docs.docker.com/install>`_
  — Install Docker for different operating system. See documentation.
* `Docker Compose <https://docs.docker.com/compose/install/>`_
  — Install Docker Compose for macOS, Windows, and Linux
* `Python 3.7 <https://www.python.org/>`_
  — Programming language
* `Django <https://www.djangoproject.com/>`_
  — The django requirements packages are in the requirements.txt file
* `Postgres <https://www.djangoproject.com/>`_
  — The user credentials are in the environment variable file called: .env
* `Nginx <https://nginx.org/en/>`_
  — Nginx is used as a reverse proxy.


Usage
=====

Build or rebuild web project:

.. code-block:: bash

    $ docker-compose build

Start web project as a service in docker.:

.. code-block:: bash

    $ docker-compose up

Stops and removes containers, networks, volumes, and images in docker.:

.. code-block:: bash

    $ docker-compose down

Create Django project:

.. code-block:: bash

    $ docker-compose run web django-admin.py startproject <project_name> .

Launch the development server:

.. code-block:: bash

    $ docker-compose run web python manage.py runserver 0.0.0.0:8000

Run makemigrations command on Django project:

.. code-block:: bash

    $ docker-compose run web python manage.py makemigrations

Run migrate command on Django project:

.. code-block:: bash

    $ docker-compose run web python manage.py migrate

Create superuser on Django project:

.. code-block:: bash

    $ docker-compose run web python manage.py createsuperuser

Run test command on Django project:

.. code-block:: bash

    $ docker-compose run web python manage.py test