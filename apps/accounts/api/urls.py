from django.urls import path

from .views import ApiRoot, UserListView, UserListCreateView, UserRetrieveDestroyView, UserCreateView, UserRetrieveView, UserDestroyView, GeocodingBaseApiView

# app_name = 'api_accounts'

urlpatterns = [
    path('', ApiRoot.as_view(), name=ApiRoot.name),
    path('list/', UserListView.as_view(), name=UserListView.name),
    path('create/', UserCreateView.as_view(), name=UserCreateView.name),
    path('user/<int:pk>/', UserRetrieveView.as_view(), name=UserRetrieveView.name),
    path('delete/<int:pk>/', UserDestroyView.as_view(), name=UserDestroyView.name),

    path('users/', UserListCreateView.as_view(), name=UserListCreateView.name),
    path('users/<int:pk>/', UserRetrieveDestroyView.as_view(), name=UserRetrieveDestroyView.name),

    path('geocoding_base/', GeocodingBaseApiView.as_view(), name=GeocodingBaseApiView.name),
]
