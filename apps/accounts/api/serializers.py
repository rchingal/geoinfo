from rest_framework import serializers

from ..models import User

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            'id',
            'first_name',
            'last_name',
            'address',
            'city',
            'latitude',
            'longitude',
            'geo_state'
        ]

    def create(self, validated_data):
        validated_data['username'] = '{}_{}'.format(validated_data['first_name'].lower(), validated_data['last_name'].lower())
        user = User.objects.create_user(**validated_data)
        return user