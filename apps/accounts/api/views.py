import requests
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from rest_framework import status
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveDestroyAPIView, CreateAPIView, RetrieveAPIView, DestroyAPIView

from .serializers import UserSerializer
from ..models import User

from apps.files.api.views import FileList

class UserCreateView(CreateAPIView):
    """
    User Create
    """
    name = 'User Create'
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserListCreateView(ListCreateAPIView):
    """
    User List or User Create
    """
    name = 'User List or User Create'
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.filter(is_superuser=False)


class UserListView(ListAPIView):
    """
    User List
    """
    name = 'User List'
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.filter(is_superuser=False)


class UserRetrieveView(RetrieveAPIView):
    """
    User detail.
    """
    name = 'User Detail'
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDestroyView(DestroyAPIView):
    """
    User delete.
    """
    name = 'User Delete'
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserRetrieveDestroyView(RetrieveDestroyAPIView):
    """
    User destroy.
    """
    name = 'User Retrieve and Destroy'
    queryset = User.objects.all()
    serializer_class = UserSerializer



class GeocodingBaseApiView(APIView):
    """
    ApiView to obtain the geocoding base given the address and city.
    """
    name = "Geocoding Base"

    def get(self, request, version):
        try:
            resp = []
            users = User.objects.filter(is_superuser=False)
            for user in users:
                if user.address and user.city:
                    try:
                        url = 'https://maps.googleapis.com/maps/api/geocode/json?address={},{}&key={}'.format(user.address, user.city, settings.API_KEY)
                        r = requests.get(url)
                        data = r.json()
                        location = data['results'][0]['geometry']['location']
                    except Exception as e:
                        location = None
                    if location:
                        user.latitude = location['lat']
                        user.longitude = location['lng']
                        user.geo_state = True
                    else:
                        user.latitude = 0
                        user.longitude = 0
                        user.geo_state = False
                    user.save()
                resp.append(user)
            serializer = UserSerializer(resp, many=True)
            return Response(serializer.data)

        except Exception as e:
            return Response({
                'status': 'error',
                'msg': _('There was an error trying to get the geocoding base.'),
                'data': str(e)
            }, status=status.HTTP_400_BAD_REQUEST)


class ApiRoot(generics.GenericAPIView):
    name = 'api-root'

    def get(self, request, *args, **kwargs):
        return Response({
            'users': reverse(UserListCreateView.name, request=request, kwargs=kwargs),
            'list': reverse(UserListView.name, request=request, kwargs=kwargs),
            'create': reverse(UserCreateView.name, request=request, kwargs=kwargs),
            'geocoding_base': reverse(GeocodingBaseApiView.name, request=request, kwargs=kwargs),
            'files': reverse(FileList.name, request=request, kwargs=kwargs),

            #'user': reverse(UserRetrieveView.name, request=request, kwargs=kwargs),
            #'delete': reverse(UserDestroyView.name, request=request, kwargs=kwargs),
        })