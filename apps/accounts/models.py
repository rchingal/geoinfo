from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.utils.translation import gettext_lazy as _
from django.db import models

class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        import random
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    """
    User model based in abstract user for profile information
    """

    address = models.CharField(max_length=100, verbose_name=_('address'),
        help_text=_('residence address.'))
    city = models.CharField(max_length=100, verbose_name=_('city'),
        help_text=_('city name.'))
    latitude = models.FloatField(blank=True, null=True, verbose_name=_('latitude'))
    longitude = models.FloatField(blank=True, null=True, verbose_name=_('longitude'))
    geo_state = models.BooleanField(default=False, verbose_name=_('geo state'),
        help_text=_('geo state'))

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        if not self.first_name or not self.last_name:
            return '{}'.format(self.email.split('@')[0])
        else:
            return '{}'.format(self.get_full_name())