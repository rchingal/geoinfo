from openpyxl import load_workbook
from celery import shared_task

from .models import Vote, File

@shared_task
def add(x,y):
    return (x+y)


@shared_task
def Upload(path):
    with open(path, 'rb') as f:
        file = f.read()
    wb = load_workbook(path)
    worksheet = wb["Sheet1"]
    excel_data = list()

    for row in worksheet.iter_rows(min_row=2, max_row=worksheet.max_row):
        row_data = list()
        for cell in row:
            row_data.append(cell.value)
        excel_data.append(row_data)
    if excel_data:
        for row in excel_data:
            try:
                departamento = row.__getitem__(0)
                municipio = row.__getitem__(1)
                zona = row.__getitem__(2)
                cod_puesto = row.__getitem__(3)
                puesto = row.__getitem__(4)
                mesa = row.__getitem__(5)
                partido = row.__getitem__(6)
                candidato = row.__getitem__(7)
                votos = row.__getitem__(8)
                latitud = row.__getitem__(9)
                longitud = row.__getitem__(10)
                _, create = Vote.objects.update_or_create(
                    departamento=departamento, municipio=municipio, zona=zona, cod_puesto=cod_puesto, puesto=puesto,
                    mesa=mesa, partido=partido, candidato=candidato, votos=votos, latitud=latitud, longitud=longitud
                )
            except Exception as e:
                pass

        votes_by_candidato = Vote.objects.all().order_by('candidato')
        votes_by_partido = Vote.objects.all().order_by('partido')
        votes_by_departamento = Vote.objects.all().order_by('departamento')
        votes_by_municipio = Vote.objects.all().order_by('municipio')

    File.objects.create(file=path)
    f.close()
    return True