from django.urls import path

from .views import FileList

# app_name = 'api_accounts'

urlpatterns = [
    path('files/', FileList.as_view(), name=FileList.name),
]
