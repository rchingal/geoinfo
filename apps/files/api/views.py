import os
from django.conf import settings
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from rest_framework import generics

from .serializers import FileSerializer
from ..models import File

from ..tasks import Upload

class FileList(generics.ListCreateAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer
    name = 'File List'

    def perform_create(self, serializer):
        file = self.request.FILES.get('file', False)
        if file:
            temp_file = default_storage.save(str(file), ContentFile(file.read()))
            tmp_file_path = os.path.join(settings.MEDIA_ROOT, temp_file)
            Upload.delay(tmp_file_path)