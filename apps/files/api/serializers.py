from rest_framework import serializers

from ..models import File


class FileSerializer(serializers.ModelSerializer):
    #file = serializers.SerializerMethodField()

    class Meta:
        model = File
        fields = ('file', )

    def get_file(self, obj):
        try:
            file = obj.file.url
        except:
            file = None
        return file