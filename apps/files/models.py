from django.db import models
from django.utils.translation import gettext_lazy as _

def generate_upload_path(instance, filename):
    return '/'.join(['upload', filename])

class File(models.Model):
    file = models.FileField(upload_to=generate_upload_path, null=True, blank=True)
    created_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return "Image: {}".format(self.id)


class Vote(models.Model):
    departamento = models.CharField(max_length=100, verbose_name=_('departamento'))
    municipio = models.CharField(max_length=100, verbose_name=_('municipio'))
    zona = models.CharField(max_length=15, verbose_name=_('zona'))
    cod_puesto = models.CharField(max_length=10, verbose_name=_('cod_puesto'))
    puesto = models.CharField(max_length=10, verbose_name=_('puesto'))
    mesa = models.CharField(max_length=10, verbose_name=_('mesa'))
    partido = models.CharField(max_length=100, verbose_name=_('partido'))
    candidato = models.CharField(max_length=100, verbose_name=_('candidato'))
    votos = models.CharField(max_length=15, verbose_name=_('votos'))
    latitud = models.FloatField(blank=True, null=True, verbose_name=_('latitud'))
    longitud = models.FloatField(blank=True, null=True, verbose_name=_('longitud'))

    class Meta:
        verbose_name = _('vote')
        verbose_name_plural = _('votes')

    def __str__(self):
        return "Vote: {} - ".format(self.id)


